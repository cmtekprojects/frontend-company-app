const faker = require('faker');
const database = { employees: [] };

for (let i=1; i<=10; i++) {
  database.employees.push({
    name: faker.name.firstName(),
    lastEventDate: faker.date.past(5, new Date(2020, 1, 1)),
  });
}
console.log(JSON.stringify(database));
