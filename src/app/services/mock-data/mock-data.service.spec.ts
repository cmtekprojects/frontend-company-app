import { TestBed } from '@angular/core/testing';

import { MockDataService } from './mock-data.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from '../../app-routing.module';

describe('HttpDataService', () => {
  let service: MockDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, AppRoutingModule ],
      providers: [ HttpClient ]
    });
    service = TestBed.inject(MockDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
