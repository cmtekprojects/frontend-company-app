import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Employee} from '../../api/model/Employee';

@Injectable({
  providedIn: 'root'
})
export class MockDataService {

  BASE_PATH = 'http://localhost:3000/employees';

  constructor(private http: HttpClient) { }

  getList(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.BASE_PATH);
  }
}
