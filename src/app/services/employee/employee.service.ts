import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmployeeResponse} from '../../api/model/EmployeeResponse';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  protected basePath = '.';

  constructor(private readonly http: HttpClient) { }

  public getEmployees(): Observable<EmployeeResponse> {
    const path = this.basePath + '/api/employees';
    return this.http.get<EmployeeResponse>(path);
  }
}
