import {ChangeDetectorRef, Component, Injectable, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Employee} from '../../api/model/Employee';
import {ActivatedRoute} from '@angular/router';
import {EmployeeService} from '../../services/employee/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
@Injectable()
export class EmployeeComponent implements OnInit {

  dataSource: MatTableDataSource<Employee>;
  displayedColumns = ['name', 'lastEventDate'];

  constructor(private readonly employeeService: EmployeeService) {
    this.dataSource = new MatTableDataSource<Employee>();
  }

  ngOnInit(): void {
    this.renderTable();
  }

  private renderTable() {
    this.employeeService.getEmployees()
      .subscribe(response => {
        this.dataSource.data = response.employees;
    });
  }

  public findNameNextEmployeeOrganizer(): string {
    return this.dataSource.data.find(employeeFinder => !employeeFinder.lastEventDate)?.name;
  }
}
