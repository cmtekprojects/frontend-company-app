import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeComponent } from './employee.component';
import {MatTableDataSource} from '@angular/material/table';
import {Employee} from '../../api/model/Employee';
import {HttpClient} from '@angular/common/http';
import {AppRoutingModule} from '../../app-routing.module';
import {EmployeeService} from '../../services/employee/employee.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('EmployeeComponent', () => {
  let component: EmployeeComponent;
  let fixture: ComponentFixture<EmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeComponent ],
      imports: [ HttpClientTestingModule, AppRoutingModule ],
      providers: [ EmployeeService, HttpClient ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title table Employee', () => {
    fixture = TestBed.createComponent(EmployeeComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Employee Listing');
  });

  it('should have table Employee', () => {
    fixture = TestBed.createComponent(EmployeeComponent);
    const table = new MatTableDataSource<Employee>();
    fixture.whenRenderingDone().then(() => {
      expect(table).toBeTruthy();
    });
  });

  it('should have title with next employee organizer name', () => {
    fixture = TestBed.createComponent(EmployeeComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').querySelector('b').textContent).toContain('Next employee organizer');
  });
});
