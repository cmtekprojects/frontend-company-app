import {Employee} from './Employee';

export interface EmployeeResponse {

  employees?: Employee[];
}
