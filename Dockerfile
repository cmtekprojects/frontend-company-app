FROM node:latest as node
MAINTAINER Cristian Moreno

ARG ENV=prod
ARG APP=frontend-company-app
ENV ENV ${ENV}
ENV APP ${APP}

WORKDIR /app
COPY ./ /app/

RUN npm start --prod
RUN mv /app/dist/${APP}/* /app/dist/

FROM nginx:1.13.8-alpine

COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
